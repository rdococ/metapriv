Metapriv mod for Minetest
=========================

The metapriv mod allows you to register a metaprivilege that grants players multiple privileges at once. To register or overwrite a metaprivilege (which requires the `privs` priv), you would use the `metapriv` command. As an example, `/metapriv spectate fly fast noclip` creates a metaprivilege `spectate` that bundles the privileges of `fly`, `fast`, and `noclip` together and allows you to grant them to players in bulk.