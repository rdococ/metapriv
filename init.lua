-- nb

local storage = minetest.get_mod_storage()

-- Internal functions to access and modify metaprivilege definitions.
local function get_metapriv_definitions()
	return minetest.deserialize(storage:get_string("data")) or {}
end
local function set_metapriv_definitions(d)
	storage:set_string("data", minetest.serialize(d))
end

local function meta_from_name(name)
	return minetest.get_player_by_name(name):get_meta()
end

-- Determine what metaprivileges allow a specific player to have specific privileges.
local function get_metaprivs_with(name, priv)
	return minetest.deserialize(meta_from_name(name):get_string("metapriv:" .. priv)) or {}
end
local function set_metaprivs_with(name, priv, d)
	meta_from_name(name):set_string("metapriv:" .. priv, minetest.serialize(d))
end

-- Callbacks for metaprivileges to apply their privileges.
local do_grant = function (name, granter, metapriv)
	local metaprivs = get_metapriv_definitions()
	
	if not metaprivs[metapriv] then return end

	local privs = minetest.get_player_privs(name)
	privs[metapriv] = true
	
	-- For the privileges that the metaprivilege offers, grant them to the player and make sure to note down that each privilege is granted by the metaprivilege.
	for _, priv in ipairs(metaprivs[metapriv].privs) do
		privs[priv] = true
		
		local pwith = get_metaprivs_with(name, priv)
		pwith[metapriv] = true
		set_metaprivs_with(name, priv, pwith)
	end
	
	minetest.after(0.1, minetest.set_player_privs, name, privs)
end
local do_revoke = function (name, granter, metapriv)
	local metaprivs = get_metapriv_definitions()
	
	if not metaprivs[metapriv] then return end

	local privs = minetest.get_player_privs(name)
	privs[metapriv] = nil
	
	-- For the privileges that this metaprivilege offers, note down that the metaprivilege no longer grants the player that privilege. If the player has no other metaprivileges that grant him a privilege, revoke it.
	for _, priv in ipairs(metaprivs[metapriv].privs) do
		local pwith = get_metaprivs_with(name, priv)
		pwith[metapriv] = nil
		set_metaprivs_with(name, priv, pwith)
		
		local test = true
		for _, v in pairs(pwith) do
			if v then test = false
			break end
		end
		
		if test then
			privs[priv] = nil
		end
	end
	
	minetest.after(0.1, minetest.set_player_privs, name, privs)
end

-- Registers the metaprivilege with the core.
local function add_metapriv(metapriv_name, privs)
	local def = get_metapriv_definitions()[metapriv_name]
	minetest.register_privilege(metapriv_name, {
		description = "Metaprivilege, includes " .. table.concat(privs, " "),
		give_to_singleplayer = false,
		
		on_grant = function (name, granter)
			do_grant(name, granter, metapriv_name)
		end,
		on_revoke = function (name, granter)
			do_revoke(name, granter, metapriv_name)
		end,
		
		[metapriv_name] = {  -- Compatibility with an error in 0.5.0-dev builtin code
			on_grant = function (name, granter)
				do_grant(name, granter, metapriv_name)
			end,
			on_revoke = function (name, granter)
				do_revoke(name, granter, metapriv_name)
			end
		}
	})
end

-- Function to register a metaprivilege and store it in mod_storage.
local function register_metapriv(name, privs)
	local def = get_metapriv_definitions()
	
	-- TODO: Proper method to modify metaprivs that updates privilege lists
	-- if def[name] then return false end
	local pexists
	
	-- By default, unlike regular privileges, metaprivileges are permanent. This is as they are intended to be used by server admins, rather than mods.
	if not def.temporary then
		pexists = def[name]
		
		def[name] = privs
		set_metapriv_definitions(def)
	end
	
	if not pexists then add_metapriv(name, privs) end
	return true
end

-- Get the list of previously registered metaprivs, and register them with the core.
for name, privs in pairs(get_metapriv_definitions()) do
	add_metapriv(name, privs)
end

-- Register a chatcommand so that server admins can register metaprivileges from within the game.
minetest.register_chatcommand("metapriv", {
	params = "<metapriv name> <priv1, priv2, ...>",
	description = "Register a metaprivilege that grants players multiple privileges at once",
	privs = {privs = true},
	func = function (name, param)
		local params = {}
		for word in param:gmatch("%w+") do table.insert(params, word) end
		
		local metapriv_name = params[1]
		table.remove(params, 1)
		
		if register_metapriv(metapriv_name, {privs = params}) then
			return true, "Success. Grant the metapriv like any normal priv."
		else
			return false, "Error."
		end
	end
})